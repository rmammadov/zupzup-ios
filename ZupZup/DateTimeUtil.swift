//
//  DateFormatterUtil.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 6/19/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

class DateTimeUtil {
    
    class func formatUnixTimeStamp(timeStamp: TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: Constant.ConfigValues.MESSAGE_DATE_ABBREVIATION) //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = Constant.ConfigValues.MESSAGE_DATE_FORTMAT //Specify your format that you want
        
        return dateFormatter.string(from: date)
    }
}
