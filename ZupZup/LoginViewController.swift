//
//  LoginViewController.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 5/26/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import UIKit
import Firebase
import TextFieldEffects

class LoginViewController: UIViewController, FirebaseLoginDelegate {

    // MARK: - Statics
    
    private static let SEGUE_SHOW_HOME: String = "showHome"
    
    // MARK: - Outlets
    
    @IBOutlet weak var txtUsername: HoshiTextField!
    
    // MARK: - Properties
    
    var firLoginHelper: FirebaseLoginHelper = FirebaseLoginHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        firLoginHelper.delegate = self
        
        // Take tap functionality and dissmis the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickLoginBtn(_ sender: Any) {
        if (txtUsername.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).characters.count)! >= Constant.ConfigValues.MIN_USERNAME_CHARS_COUNT {
            firLoginHelper.authenticateAnonymously(username: txtUsername.text!)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func onLoggedIn(loggedIn: Bool, errorMessage: String) {
        if loggedIn {
            self.performSegue(withIdentifier: LoginViewController.SEGUE_SHOW_HOME, sender: self)
        } else {
        
        }
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
