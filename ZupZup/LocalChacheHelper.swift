//
//  LocalChacheHelper.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 5/31/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import Foundation

class LocalChacheHelper {
    
    class func login(userFirKey: String, userUid: String, username: String) {
        UserDefaults.standard.setValue(userFirKey, forKey: Constant.UserDefaultKeys.USER_FIR_KEY)
        UserDefaults.standard.setValue(userUid, forKey: Constant.UserDefaultKeys.USER_FIR_UID)
        UserDefaults.standard.setValue(username, forKey: Constant.UserDefaultKeys.USER_NAME)
        UserDefaults.standard.set(true, forKey: Constant.UserDefaultKeys.USER_LOGGED_IN)
    }
    
    class func isLoggedin() -> Bool {
        return UserDefaults.standard.bool(forKey: Constant.UserDefaultKeys.USER_LOGGED_IN)
    }
    
    class func getUserFirKey() -> String {
        return UserDefaults.standard.string(forKey: Constant.UserDefaultKeys.USER_FIR_KEY)!
    }
}
