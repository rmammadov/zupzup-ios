//
//  PeopleViewModel.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 6/2/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import UIKit

protocol PeopleViewModelDelegate{
    func onListItemClick(itemPosition: Int, user: UserModel)
}

class PeopleViewModel: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    fileprivate let reuseIdentifierPeople = "cellPeople"
    
    var delegate: PeopleViewModelDelegate?
    var firRDBHelper: FirebaseRDBHelper = FirebaseRDBHelper()
    var userList = [UserModel]()
    var receivedMessageList = [MessageModel]()
    var sentMessageList = [MessageModel]()
    var tableView: UITableView?
    
    func loadUsers() {
        firRDBHelper.delegateUsers = self
        firRDBHelper.loadUsers()
    }
    
    func loadMessages() {
        firRDBHelper.delegateMessages = self
        firRDBHelper.loadReceivedMessages()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // get a reference to our storyboard cell
        let cell: PeopleTableViewCell = (tableView.dequeueReusableCell(withIdentifier: reuseIdentifierPeople) as? PeopleTableViewCell)!
        
        let user = userList[indexPath.row]
        
        if user.id == LocalChacheHelper.getUserFirKey() {
            cell.labelName.textColor = .red
        }
        
        cell.labelName.text = user.userName
        var messageCountForUser = 0
        var messagesForUser = [MessageModel]()
        
        for message in self.receivedMessageList {
            if message.senderId == user.id {
                messageCountForUser += 1
                messagesForUser.append(message)
            }
        }
        
        if messageCountForUser > 0 {
            cell.labelMessageCount.text = String(messageCountForUser)
            cell.labelMessageCount.isHidden = false
        } else {
            cell.labelMessageCount.isHidden = true
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.sendOnItemClickEvent(position: indexPath.row, userObject: userList[indexPath.row])
    }
    
    func sendOnItemClickEvent(position: Int, userObject: UserModel){
        if delegate != nil {
            delegate?.onListItemClick(itemPosition: position, user: userObject)
        }
    }
}

extension PeopleViewModel: FirebaseRDBUsersDelegate {
    
    func onUsersLoaded(status: Bool, errorMessage: String, users: [UserModel]) {
        if status {
            self.userList.removeAll()
            self.userList = users.sorted{ $0.userName! < $1.userName! }
            self.tableView?.reloadData()
            self.loadMessages()
        }
    }
}

extension PeopleViewModel: FirebaseRDBMessagesDelegate {

    func onReceivedMessagesLoaded(status: Bool, errorMessage: String, messages: [MessageModel]) {
        if status {
            self.receivedMessageList.removeAll()
            self.receivedMessageList.append(contentsOf: messages)
            self.tableView?.reloadData()
        }
    }
    
    func onSentMessagesLoaded(status: Bool, errorMessage: String, messages: [MessageModel]) {
        if status {
            self.sentMessageList.removeAll()
            self.sentMessageList.append(contentsOf: messages)
            self.tableView?.reloadData()
        }
    }
}
