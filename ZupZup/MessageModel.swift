//
//  MessageModel.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 6/4/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

class MessageModel{
    
    var id: String?
    var senderId: String?
    var receiverId: String?
    var fileLink: String?
    var date: TimeInterval
    var status: Int
    
    init(id: String?, senderId: String?, receiverId: String?, fileLink: String?, date: TimeInterval, status: Int) {
        self.id = id
        self.senderId = senderId
        self.receiverId = receiverId
        self.fileLink = fileLink
        self.date = date
        self.status = status
    }
}
