//
//  OpenEarsHeader.h
//  ZupZup
//
//  Created by Rahman Mammadov on 4/25/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

#ifndef OpenEarsHeader_h
#define OpenEarsHeader_h

#import <OpenEars/OEPocketsphinxController.h>
#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEFliteController.h>
#import <Slt/Slt.h>
#import <OpenEars/OEEventsObserver.h>
#import <OpenEars/OELogging.h>
#import <OpenEars/OEAcousticModel.h>

#endif /* OpenEarsHeader_h */
