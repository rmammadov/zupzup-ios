//
//  ViewController.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 4/19/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import UIKit
import RAMPaperSwitch
import AVFoundation

class HomeViewController: UIViewController, HomeViewModelDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate {

    // MARK: - Outlets
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var btnNoInternet: UIButton!
    @IBOutlet weak var labelSwitchMicrophone: UILabel!
    @IBOutlet weak var switchMicrophone: RAMPaperSwitch!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var btnRecordVoice: UIButton!
    @IBOutlet weak var btnPlayRecording: UIButton!
    @IBOutlet weak var btnRemoveRecording: UIButton!
    @IBOutlet weak var btnUpload: UIButton!
    
    // Recording audio with Audiokit
    var recordingSession : AVAudioSession!
    var audioRecorder    : AVAudioRecorder!
    var settings         = [String : Int]()
    var audioPlayer: AVAudioPlayer!
    
    var timer = Timer()

    // MARK: - Global declarations
    
    var openEarsEventsObserver = OEEventsObserver()
    let modelHome: HomeViewModel = HomeViewModel()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        modelHome.loadUsers()
        modelHome.setPlayBackground(status: true)
        modelHome.delegate = self
        
        // Delegate of Open ears
        self.openEarsEventsObserver.delegate = modelHome
        
        setupUi()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func setupUi() {
        self.switchMicrophone.animationDidStartClosure = {(onAnimation: Bool) in
            if self.switchMicrophone.isOn {
                self.startListening()
            } else {
                self.stopListening()
            }
        }
        
        // Take tap functionality and dissmis the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // Custom audio recording 
        setupRecording()
    }
    
    fileprivate func setUiActive() {
        self.indicator.isHidden = true
        self.btnNoInternet.isHidden = true
        self.labelSwitchMicrophone.isHidden = false
        self.switchMicrophone.isHidden = false
        self.labelStatus.isHidden = false
        self.btnRecordVoice.isHidden = false
        self.btnPlayRecording.isHidden = false
        self.btnRemoveRecording.isHidden = false
        self.btnUpload.isHidden = false
    }
    
    fileprivate func setUiPassive() {
        self.indicator.isHidden = false
        self.btnNoInternet.isHidden = false
        self.labelSwitchMicrophone.isHidden = true
        self.switchMicrophone.isHidden = true
        self.labelStatus.isHidden = true
        self.btnRecordVoice.isHidden = true
        self.btnPlayRecording.isHidden = true
        self.btnRemoveRecording.isHidden = true
        self.btnUpload.isHidden = true
    }
    
    fileprivate func startListening() {
        modelHome.startListening()
        self.labelSwitchMicrophone.text = NSLocalizedString("Listening", comment: " ")
        modelHome.statusTextColor = self.labelSwitchMicrophone.textColor.cgColor
        self.labelSwitchMicrophone.textColor = .white
    }
    
    fileprivate func stopListening() {
        modelHome.stopListening()
        self.labelSwitchMicrophone.text = NSLocalizedString("Not listening", comment: " ")
        self.labelSwitchMicrophone.textColor = UIColor(cgColor: modelHome.statusTextColor!)
    }
    
    // -------------------------------------------------------------------------
    
    // MARK: - start recoridng
    
    fileprivate func setupRecording() {
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() {allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print("Allow")
                    } else {
                        print("Dont Allow")
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
        
        // Audio Settings
        
        settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
    }
    
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let soundURL = documentDirectory.appendingPathComponent("sound.m4a")
        return soundURL as NSURL?
    }
    
    func startRecording() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,
                                                settings: settings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
        } catch {
            
        }
        
        self.btnRecordVoice.setImage(UIImage(named: "ic_stop"), for: .normal)
        self.labelStatus.text = NSLocalizedString("Recording", comment: " ")
        
        self.timer = Timer.scheduledTimer(withTimeInterval: Constant.ConfigValues.MAX_DURATION_OF_RECORDING, repeats: false){_ in
           self.finishRecording(success: true)
        }
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        if success {
            print(success)
        } else {
            audioRecorder = nil
            print("Somthing Wrong.")
        }
        
        self.btnRecordVoice.setImage(UIImage(named: "ic_record"), for: .normal)
        self.labelStatus.text = NSLocalizedString("Recording finished, click to upload to send the voice message", comment: " ")
        
        self.timer.invalidate()
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        } else {
            self.labelStatus.text = String(describing: audioRecorder.url)
        }
    }

    func playRecordedAudio() {
        if audioRecorder != nil {
            if !audioRecorder.isRecording {
                if self.audioPlayer == nil {
                    self.audioPlayer = try! AVAudioPlayer(contentsOf: audioRecorder.url)
                    self.audioPlayer.prepareToPlay()
                    self.audioPlayer.delegate = self
                }
                self.audioPlayer.play()
            }
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print(flag)
        self.btnPlayRecording.setImage(UIImage(named: "ic_play"), for: .normal)
        self.audioPlayer = nil
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?){
        print(error.debugDescription)
    }
    
    internal func audioPlayerBeginInterruption(_ player: AVAudioPlayer){
        print(player.debugDescription)
        self.btnPlayRecording.setImage(UIImage(named: "ic_play"), for: .normal)
        self.audioPlayer = nil
    }
    
    // -------------------------------------------------------------------------
    
    func toggleRecording() {
        if audioRecorder == nil {
            self.startRecording()
        } else {
            self.finishRecording(success: true)
        }
    }
    
    func togglePlay() {
        if audioRecorder != nil {
            if self.audioPlayer != nil && self.audioPlayer.isPlaying {
                self.btnPlayRecording.setImage(UIImage(named: "ic_play"), for: .normal)
                self.audioPlayer.pause()
            } else {
                self.btnPlayRecording.setImage(UIImage(named: "ic_pause"), for: .normal)
                self.playRecordedAudio()
            }
        }
    }
    
    // MARK: - Ui actions
    
    @IBAction func onClickPlayButton(_ sender: Any) {
        self.togglePlay()
    }
    
    @IBAction func onClickBtnRecord(_ sender: Any) {
        self.toggleRecording()
    }
    
    @IBAction func onClickBtnRemoveRecording(_ sender: Any) {
        audioRecorder = nil
        self.labelStatus.text = " "
    }
    
    @IBAction func onClickUpload(_ sender: Any) {
//        modelHome.uploadTheSound(fileUrl: audioRecorder.url)
//        audioRecorder = nil
//        self.labelStatus.text = " "
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.sendPush(token: " ", payload: "Test")
    }
    
    // MARK: - dissmis the keyboard
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK: - ModelView delegate methods
    
    func didCreateLanguageModel(status: Bool, message: String) {
        self.setUiActive()
    }
    
    func didRecieveHypothesis(hypothesis: String) {
    
    }
    
    func didFinishSpeech() {
//        self.toggleRecording()
    }
    
    func didStartRecording() {
        self.toggleRecording()
    }
    
    func didFinishRecording(locationOfTheFile: String) {
    }
}

