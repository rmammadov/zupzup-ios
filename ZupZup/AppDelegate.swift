//
//  AppDelegate.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 4/19/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import UIKit
import CoreData
import PushKit
import UserNotifications
import Firebase
import NWPusher

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PKPushRegistryDelegate {

    static let IDENTIFIER_STORYBOARD_MAIN = "Main"
    static let IDENTIFIER_VIEWCONTROLLER_LOGIN = "LoginViewControllerIdentifier"
    static let IDENTIFIER_VIEWCONTROLLER_MAIN_TABS = "MainTabBarControllerIdentifier"
    static let INDEX_MAIN_VIEW = 0
    static let INDEX_CONVERSATION_VIEW = 1
    
    var window: UIWindow?
    var center: UNUserNotificationCenter? = nil
    var options: UNAuthorizationOptions? = nil
    let zzNotificationDelegate = ZZNotificationModel()
    private let _pushRegistry = PKPushRegistry(queue: DispatchQueue.main)
    private var audioUtil = AudioUtil()
    private var pusher: NWPusher? = nil
    private var tokenForTest = " " // for testing

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure() // Firebase initalization
        
        // Notification center
        center = UNUserNotificationCenter.current() // Notification center
        center?.delegate = zzNotificationDelegate // Delegate handler
        
        // Asking for permission
        options = [.alert, .sound, .badge];
        registerVoIPPush() // Register for Pushkit
        
        // Check and set root View controller
        self.setFirstViewController(launchOptionsData: launchOptions)
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: Constant.ConfigValues.NAME_OF_PERSISTENT_CONTAINER) // Name of the Persistent Container
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: - Voip push notification
    
    func registerVoIPPush() {
        // Ask for authorization 
        self.requestAuthorization()
        // PushKit
        _pushRegistry.delegate = self
        var typeSet = Set<PKPushType>()
        typeSet.insert(.voIP)
        _pushRegistry.desiredPushTypes = typeSet
    }
    
    //MARK: - Delegates -
    func pushRegistry(_ registry: PKPushRegistry, didUpdate credentials: PKPushCredentials,  forType type: PKPushType) {
         let token = credentials.token.map { String(format: "%02.2hhx", $0) }.joined()
         if type == PKPushType.voIP {
            print("pushRegistry didUpdate credentials \(token) length: \(credentials.token.count)")
        }
        
        self.tokenForTest = token // For testing
        print(self.tokenForTest)
        self.updateToken(apnsToken: token)
        self.makeConnectionNwPush()
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenForType type: PKPushType) {
        print("pushRegistry didInvalidatePushTokenForType")
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, forType type: PKPushType) {
        print("pushRegistry didReceiveIncomingPushWith")
        let payloadDict = payload.dictionaryPayload["aps"] as? Dictionary<String, String>
        let message = payloadDict?["alert"]
        
        // Show local notification
        self.showNotification(message: message)
    }

    func showNotification(message: String?) {
//        self.audioUtil.downloadFileFromURLandPlay(urlString: message!)
        //present a local notifcation to visually see when we are recieving a VoIP Notification
    //    if UIApplication.shared.applicationState == UIApplicationState.background {
            let content = UNMutableNotificationContent()
            content.title = "New Zup" // Should be overrided by incoming notificaiton title or based on message
            content.body = message!
            content.sound = UNNotificationSound.default()
            
            // Schedule a notification for a number of seconds later.
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1,
                                                            repeats: false)
            
            let identifier = Constant.Keys.IDENTIFIER_LOCAL_NOTIFICATON
            let request = UNNotificationRequest(identifier: identifier,
                                                content: content, trigger: trigger)
            center?.add(request, withCompletionHandler: { (error) in
                if error != nil {
                    // Something went wrong
                }
            })
            
            let snoozeAction = UNNotificationAction(identifier: "Snooze",
                                                    title: "Snooze", options: [])
            let deleteAction = UNNotificationAction(identifier: "ZZDeleteAction",
                                                    title: "Delete", options: [.destructive])
            
            let category = UNNotificationCategory(identifier: "ZZReminderCategory",
                                                  actions: [snoozeAction,deleteAction],
                                                  intentIdentifiers: [], options: [])

            center?.setNotificationCategories([category])
            UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
     /*   } else {
            print("Notitification received while in foreground")
            self.showDialog(viewController: self.getActiveViewController(), title: "New Zup", body: message!) // Should be overrided by incoming notificaiton title or based on message
        } */
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    
    func requestAuthorization() {
        center?.requestAuthorization(options: options!) {
            (granted, error) in
            if !granted {
                
            } else {
                self.registerVoIPPush()
            }
        }
    }
    
    func showDialog(viewController: UIViewController, title: String, body: String) {
        //It takes the title and the alert message and prefferred style
        let alertController = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: " "), style: .default, handler: nil)
        alertController.addAction(defaultAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    func getActiveViewController() -> UIViewController {
         return UIApplication.shared.windows[0].rootViewController as! UITabBarController
    }
    
    func setFirstViewController(launchOptionsData: [UIApplicationLaunchOptionsKey: Any]?) {
        // Check if user not logged in set root view as login view controller
        if (!LocalChacheHelper.isLoggedin()) {
            let signInView = UIStoryboard(name: AppDelegate.IDENTIFIER_STORYBOARD_MAIN, bundle: Bundle.main).instantiateViewController(withIdentifier: AppDelegate.IDENTIFIER_VIEWCONTROLLER_LOGIN) as? LoginViewController
            self.window!.rootViewController = signInView;
        } else {
            if let option = launchOptionsData {
                let info = option [UIApplicationLaunchOptionsKey.remoteNotification]
                let mainTabsView = UIStoryboard(name: AppDelegate.IDENTIFIER_STORYBOARD_MAIN, bundle: Bundle.main).instantiateViewController(withIdentifier: AppDelegate.IDENTIFIER_VIEWCONTROLLER_MAIN_TABS) as? UITabBarController
                if (info != nil) {
                    mainTabsView?.selectedIndex = AppDelegate.INDEX_CONVERSATION_VIEW
                } else {
                    mainTabsView?.selectedIndex = AppDelegate.INDEX_MAIN_VIEW
                }
                self.window!.rootViewController = mainTabsView;
            }
        }
    }
    
    func updateToken(apnsToken: String?){
        if LocalChacheHelper.isLoggedin() {
            let frdb: FirebaseRDBHelper = FirebaseRDBHelper()
            let userFirKey = LocalChacheHelper.getUserFirKey()
            frdb.insertToken(key: userFirKey, token: apnsToken)
        }
    }
    
    func makeConnectionNwPush() {
        let environment: NWEnvironment = NWEnvironment.sandbox
        let url: URL = Bundle.main.url(forResource: "zup_cer", withExtension: "p12")!
        let pkcs12: Data =  try! Data(contentsOf: url)
        print("PKCS\(pkcs12)")
        if self.pusher == nil {
            pusher = try! NWPusher.connect(withPKCS12Data: pkcs12, password: "ZupZup2018", environment: environment)
        }
        
        if (pusher != nil) {
            print("Connection made")
        } else {
            print("Connection not made")
        }
    }
    
    func sendPush(token: String, payload: String) {
        var pushed: Bool = false
        let generatedIdentifier = UInt(arc4random_uniform(42))
        do {
            pushed = ((try pusher?.pushPayload(payload, token: token, identifier: generatedIdentifier)) != nil)
        } catch {
            //handle error
            print(error)
        }
        
        if pushed {
            print("Push has been sent")
        } else {
            print("Push couldn't send")
        }
    }
}
