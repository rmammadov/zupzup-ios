//
//  PeopleViewController.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 6/2/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import UIKit

class PeopleViewController: UIViewController {

    // MARK: - Statics
    
    static let SHOW_CONVERSATION = "showConversation"
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Decelrations
    
    var modelPeople = PeopleViewModel()
    var user: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        modelPeople.delegate = self
        tableView.delegate = modelPeople
        tableView.dataSource = modelPeople
        modelPeople.tableView = tableView
        modelPeople.loadUsers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let conversationVC = segue.destination as! ConversationViewController
        conversationVC.user = self.user
    }
}

extension PeopleViewController: PeopleViewModelDelegate {
    func onListItemClick(itemPosition: Int, user: UserModel) {
        self.user = user
        performSegue(withIdentifier: PeopleViewController.SHOW_CONVERSATION, sender: self)
    }
}
