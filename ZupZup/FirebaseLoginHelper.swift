//
//  FirebaseLoginHelper.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 5/28/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import Firebase

protocol FirebaseLoginDelegate {
    func onLoggedIn(loggedIn: Bool, errorMessage: String)
}

class FirebaseLoginHelper {
    
    var delegate: FirebaseLoginDelegate?
    var firDBHelper = FirebaseRDBHelper()
    
    // MARK: - Fir authenticate anonymously
    
    func authenticateAnonymously(username: String) {
        Auth.auth().signInAnonymously() { (user, error) in
            if let err = error {
                print(err.localizedDescription)
                self.sendLoginStatus(loginStatus: false, errorMessage: " ")
                return
            }
            
            _ = user!.isAnonymous  // true
            let userUid: String = user!.uid
            
            self.firDBHelper.insertUser(uId: userUid, username: username)
            self.sendLoginStatus(loginStatus: true, errorMessage: " ")
        }
    }
    
    func sendLoginStatus(loginStatus: Bool, errorMessage: String) {
        if delegate != nil {
            delegate?.onLoggedIn(loggedIn: loginStatus, errorMessage: errorMessage)
        }
    }
}
