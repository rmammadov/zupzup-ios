//
//  HomeViewModel.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 4/28/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import UIKit

// MARK: - Delegate protocola and methods

protocol HomeViewModelDelegate {
    func didCreateLanguageModel(status: Bool, message: String)
    func didRecieveHypothesis(hypothesis: String)
    func didFinishSpeech()
    func didStartRecording()
    func didFinishRecording(locationOfTheFile: String)
}

class HomeViewModel: NSObject {
    
    // MARK: - Global declarations
    
    var delegate: HomeViewModelDelegate? = nil
    
    fileprivate var fliteController = OEFliteController()
    fileprivate var slt = Slt()
    fileprivate let lmGenerator = OELanguageModelGenerator()
    
    fileprivate var nameOfVocabulary: String = " "
    fileprivate var arrayWords: Array<Any> = []
    fileprivate var arrayObjects: Array<UserModel> = []
    fileprivate var receiverId: String = " "
    fileprivate var receiverToken: String = " "
    
    fileprivate var lmPath: String = " "
    fileprivate var dicPath: String = " "
    
    var statusTextColor: CGColor? = nil
    
    var firRDBHelper: FirebaseRDBHelper = FirebaseRDBHelper()
    var firStorageHelper = FirebaseStorageHelper()
    
    var shouldRecord = false

    // MARK: - Init function
    
    func loadUsers() {
        firRDBHelper.delegateUsers = self
        firRDBHelper.loadUsers()
    }
    
    // MARK: - Create model of phrases
    
    fileprivate func createLanguageModel() {
        let err: Error! = lmGenerator.generateLanguageModel(from: self.arrayWords, withFilesNamed: self.nameOfVocabulary, forAcousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish"))
        
        if(err != nil) {
            print("Error while creating initial language model: \(err)")
            self.notifyCallback(callbackCode: 1, status: false, message: "Error while creating initial language model: \(err)", locationOfTheFile: " ")
        } else {
            lmPath = lmGenerator.pathToSuccessfullyGeneratedLanguageModel(withRequestedName: self.nameOfVocabulary) // Convenience method to reference the path of a language model known to have been created successfully.
            dicPath = lmGenerator.pathToSuccessfullyGeneratedDictionary(withRequestedName: self.nameOfVocabulary) // Convenience method to reference the path of a dictionary known to have been created successfully.
            
            
            // OELogging.startOpenEarsLogging() //Uncomment to receive full OpenEars logging in case of any unexpected results.
            do {
                try OEPocketsphinxController.sharedInstance().setActive(true) // Setting the shared OEPocketsphinxController active is necessary before any of its properties are accessed.
                self.notifyCallback(callbackCode: 1, status: true, message: " ", locationOfTheFile: " ")
            } catch {
                print("Error: it wasn't possible to set the shared instance to active: \"\(error)\"")
                self.notifyCallback(callbackCode: 1, status: false, message: "Error: it wasn't possible to set the shared instance to active: \"\(error)\"", locationOfTheFile: " ")
            }
        }
    }

    
    // MARK: - Start listening
    
    func startListening() {
        if(!OEPocketsphinxController.sharedInstance().isListening) {
            OEPocketsphinxController.sharedInstance().startListeningWithLanguageModel(atPath: lmPath, dictionaryAtPath: dicPath, acousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish"), languageModelIsJSGF: false)
        }
    }
    
     // MARK: - Stop listening
    
    func stopListening() {
        if(OEPocketsphinxController.sharedInstance().isListening){
            let stopListeningError: Error! = OEPocketsphinxController.sharedInstance().stopListening()
            if(stopListeningError != nil) {
                print("Error while stopping listening in pocketsphinxFailedNoMicPermissions: \(stopListeningError)")
            }
        }
    }
    
    // MARK: - Say recognized phrase
    
    func say(text: String) {
         self.fliteController.say(_:"You wrote \(text)", with:self.slt)
    }
    
    // MARK: - Play sound
    
    func playSound(url: String) {

    }
    
    // MARK: - Set background play mode
    
    func setPlayBackground(status: Bool) {
        if status {
            AudioUtil.setAccessPlayBackground()
        }
    }
    
    func uploadTheSound(fileUrl: URL) {
        firStorageHelper.delegate = self
        let key = firRDBHelper.getAutoKeyForMessage()
        firStorageHelper.uploadToFireBase(recordedFile: fileUrl, key: key)
    }
    
    // MARK: - Hanlde delegates
    
    fileprivate func notifyCallback(callbackCode: Int ,status: Bool, message: String, locationOfTheFile: String) {
        if self.delegate != nil {
            switch callbackCode {
                case 1:
                    delegate?.didCreateLanguageModel(status: status, message: message)
                case 2:
                    delegate?.didRecieveHypothesis(hypothesis: message)
                case 3:
                    delegate?.didStartRecording()
                case 4:
                    delegate?.didFinishRecording(locationOfTheFile: locationOfTheFile)
                case 5:
                    delegate?.didFinishSpeech()
                default:
                    break
                }
        }
    }
}

// MARK: - Delegate methods of OpenEars and voice recording plugin

extension HomeViewModel: OEEventsObserverDelegate {
    
    func pocketsphinxDidReceiveHypothesis(_ hypothesis: String!, recognitionScore: String!, utteranceID: String!) { // Something was heard
        print("Local callback: The received hypothesis is \(hypothesis!) with a score of \(recognitionScore!) and an ID of \(utteranceID!)")
        self.shouldRecord = false
        self.notifyCallback(callbackCode: 2, status: true, message: hypothesis, locationOfTheFile: " ")
        
        if let index = self.arrayWords.index(where: { String(describing: $0).lowercased() == hypothesis.lowercased()}) {
           let indexSubstring = hypothesis.index(hypothesis.startIndex, offsetBy: 4)
           let userName = hypothesis.substring(from: indexSubstring)
           self.fliteController.say(_:"Starting to record audio message for \(userName)", with:self.slt)
           let userModel = self.arrayObjects[index]
           self.receiverId = userModel.id!
           self.receiverToken = userModel.apnsToken!
           print(self.receiverId)
           self.shouldRecord = true
        }
    }
    
    // An optional delegate method of OEEventsObserver which informs that the Pocketsphinx recognition loop has entered its actual loop.
    // This might be useful in debugging a conflict between another sound class and Pocketsphinx.
    func pocketsphinxRecognitionLoopDidStart() {
        print("Local callback: Pocketsphinx started.") // Log it.
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx is now listening for speech.
    func pocketsphinxDidStartListening() {
        print("Local callback: Pocketsphinx is now listening.") // Log it.
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx detected speech and is starting to process it.
    func pocketsphinxDidDetectSpeech() {
        print("Local callback: Pocketsphinx has detected speech.") // Log it.
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx detected a second of silence, indicating the end of an utterance.
    func pocketsphinxDidDetectFinishedSpeech() {
        print("Local callback: Pocketsphinx has detected a second of silence, concluding an utterance.") // Log it.
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx has exited its recognition loop, most
    // likely in response to the OEPocketsphinxController being told to stop listening via the stopListening method.
    func pocketsphinxDidStopListening() {
        print("Local callback: Pocketsphinx has stopped listening.") // Log it.
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx is still in its listening loop but it is not
    // Going to react to speech until listening is resumed.  This can happen as a result of Flite speech being
    // in progress on an audio route that doesn't support simultaneous Flite speech and Pocketsphinx recognition,
    // or as a result of the OEPocketsphinxController being told to suspend recognition via the suspendRecognition method.
    func pocketsphinxDidSuspendRecognition() {
        print("Local callback: Pocketsphinx has suspended recognition.") // Log it.
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx is still in its listening loop and after recognition
    // having been suspended it is now resuming.  This can happen as a result of Flite speech completing
    // on an audio route that doesn't support simultaneous Flite speech and Pocketsphinx recognition,
    // or as a result of the OEPocketsphinxController being told to resume recognition via the resumeRecognition method.
    func pocketsphinxDidResumeRecognition() {
        print("Local callback: Pocketsphinx has resumed recognition.") // Log it.
    }
    
    // An optional delegate method which informs that Pocketsphinx switched over to a new language model at the given URL in the course of
    // recognition. This does not imply that it is a valid file or that recognition will be successful using the file.
    func pocketsphinxDidChangeLanguageModel(toFile newLanguageModelPathAsString: String!, andDictionary newDictionaryPathAsString: String!) {
        print("Local callback: Pocketsphinx is now using the following language model: \n\(newLanguageModelPathAsString!) and the following dictionary: \(newDictionaryPathAsString!)")
    }
    
    // An optional delegate method of OEEventsObserver which informs that Flite is speaking, most likely to be useful if debugging a
    // complex interaction between sound classes. You don't have to do anything yourself in order to prevent Pocketsphinx from listening to Flite talk and trying to recognize the speech.
    func fliteDidStartSpeaking() {
        print("Local callback: Flite has started speaking") // Log it.
    }
    
    // An optional delegate method of OEEventsObserver which informs that Flite is finished speaking, most likely to be useful if debugging a
    // complex interaction between sound classes.
    func fliteDidFinishSpeaking() {
        print("Local callback: Flite has finished speaking") // Log it.
        if self.shouldRecord {
            self.notifyCallback(callbackCode: 3, status: true, message: " ", locationOfTheFile: " ")
        }
        self.notifyCallback(callbackCode: 5, status: true, message: " ", locationOfTheFile: " ")
        self.shouldRecord = false
    }
    
    func pocketSphinxContinuousSetupDidFail(withReason reasonForFailure: String!) { // This can let you know that something went wrong with the recognition loop startup. Turn on [OELogging startOpenEarsLogging] to learn why.
        print("Local callback: Setting up the continuous recognition loop has failed for the reason \(reasonForFailure), please turn on OELogging.startOpenEarsLogging() to learn more.") // Log it.
    }
    
    func pocketSphinxContinuousTeardownDidFail(withReason reasonForFailure: String!) { // This can let you know that something went wrong with the recognition loop startup. Turn on OELogging.startOpenEarsLogging() to learn why.
        print("Local callback: Tearing down the continuous recognition loop has failed for the reason \(reasonForFailure)") // Log it.
    }
    
    /** Pocketsphinx couldn't start because it has no mic permissions (will only be returned on iOS7 or later).*/
    func pocketsphinxFailedNoMicPermissions() {
        print("Local callback: The user has never set mic permissions or denied permission to this app's mic, so listening will not start.")
    }
    
    /** The user prompt to get mic permissions, or a check of the mic permissions, has completed with a true or a false result  (will only be returned on iOS7 or later).*/
    
    func micPermissionCheckCompleted(withResult: Bool) {
        print("Local callback: mic check completed.")
    }

    func wavWasSaved(atLocation location: String!) {
        print("wav was saved at location \(location!)")
//        self.notifyCallback(callbackCode: 4, status: true, message: " ", locationOfTheFile: location)
    }
}

extension HomeViewModel: FirebaseRDBUsersDelegate {
    
    func onUsersLoaded(status: Bool, errorMessage: String, users: [UserModel]) {
        if status {
            self.nameOfVocabulary = Constant.OpenEars.NAME_OF_VOCABULARY
            self.arrayWords.removeAll()
            self.arrayObjects.removeAll()
            for userObject in users {
                self.arrayWords.append(Constant.OpenEars.VOICE_RECO_KEY_WORD + userObject.userName!)
            }
            self.arrayObjects = users
            createLanguageModel()
        }
    }
}

extension HomeViewModel: FirebaseStorageDelegate {
    
    func onFileUploaded(status: Bool, errorMessage: String, fileLink: String, key: String) {
        if status {
            self.firRDBHelper.delegateNewMessageSent = self
            let userId = LocalChacheHelper.getUserFirKey()
            self.firRDBHelper.insertMessage(key: key, senderId: userId, receiverId: self.receiverId, fileLink: fileLink, status: 1)
        }
    }
}

extension HomeViewModel: FirebaseRDBNewMessageSentDelegate {
    
    func onMessageSent(status: Bool, messageId: String, receiverId: String, messageTitle: String, fileLink: String) {
        if status {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.sendPush(token: self.receiverToken, payload: fileLink)
        }
    }
}


