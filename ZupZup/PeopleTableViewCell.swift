//
//  PeopleTableViewCell.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 6/2/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import UIKit

class PeopleTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelMessageCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
