//
//  PlaySoundUtil.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 4/30/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

protocol AudioUtilDelegate {
    func didAudioPlayFinished()
    func didAudioPlayInterrupted()
}

class AudioUtil: NSObject, AVAudioPlayerDelegate {
    
    var delegate: AudioUtilDelegate?
    var audioPlayer: AVAudioPlayer!
    
    // MARK: - Set background play access
    
    class func setAccessPlayBackground() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSession is Active")
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
    
    // MARK: - Download the audio
    
    func downloadFileFromURLandPlay(urlString: String){
        let urlstring = urlString
        let url = NSURL(string: urlstring)
        
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url! as URL, completionHandler: { [weak self](URL, response, error) -> Void in
            self?.playAudio(url: URL!)
        })
        
        downloadTask.resume()
    }
    
    // MARK: - Play sound
    
    func playAudio(url: URL) {
        if  self.audioPlayer == nil {
            self.audioPlayer = try! AVAudioPlayer(contentsOf: url)
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.delegate = self
        }
        audioPlayer.play()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print(flag)
        self.audioPlayer = nil
        self.notifyAudioCallback(keyCallback: 1)
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?){
        print(error.debugDescription)
    }
    
    internal func audioPlayerBeginInterruption(_ player: AVAudioPlayer){
        print(player.debugDescription)
        self.audioPlayer = nil
        self.notifyAudioCallback(keyCallback: 2)
    }
    
    fileprivate func notifyAudioCallback(keyCallback: Int) {
        if self.delegate != nil {
            switch(keyCallback){
                case 1:
                    delegate?.didAudioPlayFinished()
                case 2:
                    delegate?.didAudioPlayInterrupted()
                default:
                    break
            }
        }
    }
}
