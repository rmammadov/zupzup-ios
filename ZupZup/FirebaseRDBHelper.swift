//
//  FirebaseRDBHelper.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 6/2/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import Firebase

protocol FirebaseRDBUsersDelegate {
    func onUsersLoaded(status: Bool, errorMessage: String, users: [UserModel])
}

protocol FirebaseRDBNewMessageSentDelegate {
    func onMessageSent(status: Bool, messageId: String, receiverId: String, messageTitle: String, fileLink: String)
}

protocol FirebaseRDBMessagesDelegate {
    func onReceivedMessagesLoaded(status: Bool, errorMessage: String, messages: [MessageModel])
    func onSentMessagesLoaded(status: Bool, errorMessage: String, messages: [MessageModel])
}

class FirebaseRDBHelper {
    
    // MARK: - Statistics
    
    static let KEY_RECEIVED_MESSAGES = 1
    static let KEY_SENT_MESSAGES = 2
    
    // MARK: - Global declarations
    
    var delegateUsers: FirebaseRDBUsersDelegate?
    var delegateNewMessageSent: FirebaseRDBNewMessageSentDelegate?
    var delegateMessages: FirebaseRDBMessagesDelegate?
    var refUsers: DatabaseReference!
    var refMessages: DatabaseReference!
    
    // MARK: - Users
    
    func getUserGroupReference() {
        if refUsers == nil {
            refUsers = Database.database().reference().child(Constant.Firebase.DIR_USERS);
        }
    }
    
    func getAutoKeyForUser() -> String {
        self.getUserGroupReference()
        return refUsers.childByAutoId().key
    }
    
    func insertUser(uId: String, username: String) {
        self.getUserGroupReference()
        var key = self.getAutoKeyForUser()
        
        refUsers.queryOrdered(byChild: Constant.Firebase.KEY_USER_UID).queryEqual(toValue: uId).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.exists() {
                // User already exists and update the username only
                for user in snapshot.children.allObjects as! [DataSnapshot]{
                    let userObject = user.value as? [String: AnyObject]
                    key  = userObject?[Constant.Firebase.KEY_USER_ID] as! String
                    self.refUsers.child(key).updateChildValues([Constant.Firebase.KEY_USER_NAME: username])
                    LocalChacheHelper.login(userFirKey: key, userUid: uId, username: username)
                }
            }else{
                // User has not exist and insert it to the base
                let user = [Constant.Firebase.KEY_USER_ID: key,
                            Constant.Firebase.KEY_USER_NAME: username,
                            Constant.Firebase.KEY_USER_UID: uId,
                            Constant.Firebase.KEY_OS_NAME: Constant.ConfigValues.DEVICE_OS,
                            Constant.Firebase.KEY_NOTIFICATION_TOKEN: " "
                ]
                
                //adding the artist inside the generated unique key
                self.refUsers.child(key).setValue(user)
                LocalChacheHelper.login(userFirKey: key, userUid: uId, username: username)
            }
        })
    }
    
    func insertToken(key: String, token: String?) {
        self.getUserGroupReference()
        self.refUsers.child(key).updateChildValues([Constant.Firebase.KEY_NOTIFICATION_TOKEN: token ?? " "])
    }
    
     func loadUsers() {
        self.getUserGroupReference()
        var userList = [UserModel]()
        
        //observing the data changes
        refUsers.observe(DataEventType.value, with: { (snapshot) in
            //if the reference have some values
            userList.removeAll()
            if snapshot.exists() {
                //iterating through all the values
                for user in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let userObject = user.value as? [String: AnyObject]
                    let userId: String?  = userObject?[Constant.Firebase.KEY_USER_ID] as? String
                    let userUId: String?  = userObject?[Constant.Firebase.KEY_USER_UID] as? String
                    let userName: String? = userObject?[Constant.Firebase.KEY_USER_NAME] as? String
                    let apnsToken: String? = userObject?[Constant.Firebase.KEY_NOTIFICATION_TOKEN] as? String
                    
                    //creating user object with model and fetched values
                    let user = UserModel(id: userId, uId: userUId, userName: userName, apnsToken: apnsToken)
                    
                    //appending it to list
                    userList.append(user)
                }
            }
            
            self.sendUsersLoadStatus(status: true, errorMessage: " ", users: userList)
        })
     }
    
     func sendUsersLoadStatus(status: Bool, errorMessage: String, users: Array<UserModel>) {
        if delegateUsers != nil {
            delegateUsers?.onUsersLoaded(status: status, errorMessage: errorMessage, users: users)
        }
     }
    
    // MARK: - Messages
    
    func getMessagesReference() {
        if refMessages == nil {
            refMessages = Database.database().reference().child(Constant.Firebase.DIR_MESSAGES);
        }
    }
    
    func getAutoKeyForMessage() -> String {
        self.getMessagesReference()
        return refMessages.childByAutoId().key
    }
    
    func insertMessage(key: String, senderId: String, receiverId: String, fileLink: String, status: Int) {
        self.getMessagesReference()
        let message = [Constant.Firebase.KEY_MESSAGE_ID: key,
                        Constant.Firebase.KEY_MESSAGE_SENDER_ID: senderId,
                        Constant.Firebase.KEY_MESSAGE_RECEIVER_ID: receiverId,
                        Constant.Firebase.KEY_MESSAGE_FILE_LINK: fileLink,
                        Constant.Firebase.KEY_MESSAGE_DATE: ServerValue.timestamp(),
                        Constant.Firebase.KEY_MESSAGE_STATUS: status
        ] as [String : Any]
        
        //adding the artist inside the generated unique key
        refMessages.child(key).setValue(message)
        
        self.sendNewMessageSentStatus(status: true, messageId: key, receiverId: receiverId, messageTitle: "New Message", fileLink: fileLink)
    }
    
    
    func sendNewMessageSentStatus(status: Bool, messageId: String, receiverId: String, messageTitle: String, fileLink: String) {
        if delegateNewMessageSent != nil {
            self.delegateNewMessageSent?.onMessageSent(status: status, messageId: messageId, receiverId: receiverId, messageTitle: messageTitle, fileLink: fileLink)
        }
    }
    
    func loadReceivedMessages() {
        self.getMessagesReference()
        var messagesList = [MessageModel]()
        let userId: String = LocalChacheHelper.getUserFirKey()
        
        self.refMessages.queryOrdered(byChild: Constant.Firebase.KEY_MESSAGE_RECEIVER_ID).queryEqual(toValue: userId).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            messagesList.removeAll()
            if snapshot.exists() {
                //iterating through all the values
                for message in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let messageObject = message.value as? [String: AnyObject]
                    let messageId  = messageObject?[Constant.Firebase.KEY_MESSAGE_ID] as! String
                    let senderId  = messageObject?[Constant.Firebase.KEY_MESSAGE_SENDER_ID] as! String
                    let receiverId  = messageObject?[Constant.Firebase.KEY_MESSAGE_RECEIVER_ID] as! String
                    let fileLink  = messageObject?[Constant.Firebase.KEY_MESSAGE_FILE_LINK] as! String
                    let date  = messageObject?[Constant.Firebase.KEY_MESSAGE_DATE] as! TimeInterval
                    let status  = messageObject?[Constant.Firebase.KEY_MESSAGE_STATUS] as! Int
                    
                    //creating user object with model and fetched values
                    let message = MessageModel(id: messageId, senderId: senderId, receiverId: receiverId, fileLink: fileLink, date: date, status: status)
                    
                    //appending it to list
                    messagesList.append(message)
                }
                self.sendMessagesLoadStatus(key: FirebaseRDBHelper.KEY_RECEIVED_MESSAGES, status: true, errorMessage: " ", messages: messagesList)
            }
        })
    }
    
    func loadSentMessages() {
        self.getMessagesReference()
        var messagesList = [MessageModel]()
        let userId = LocalChacheHelper.getUserFirKey()
        
        self.refMessages.queryOrdered(byChild: Constant.Firebase.KEY_MESSAGE_SENDER_ID).queryEqual(toValue: userId).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            messagesList.removeAll()
            if snapshot.exists() {
                //iterating through all the values
                for message in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let messageObject = message.value as? [String: AnyObject]
                    let messageId  = messageObject?[Constant.Firebase.KEY_MESSAGE_ID] as! String
                    let senderId  = messageObject?[Constant.Firebase.KEY_MESSAGE_SENDER_ID] as! String
                    let receiverId  = messageObject?[Constant.Firebase.KEY_MESSAGE_RECEIVER_ID] as! String
                    let fileLink  = messageObject?[Constant.Firebase.KEY_MESSAGE_FILE_LINK] as! String
                    let date  = messageObject?[Constant.Firebase.KEY_MESSAGE_DATE] as! TimeInterval
                    let status  = messageObject?[Constant.Firebase.KEY_MESSAGE_STATUS] as! Int
                    
                    //creating user object with model and fetched values
                    let message = MessageModel(id: messageId, senderId: senderId, receiverId: receiverId, fileLink: fileLink, date: date, status: status)
                    
                    //appending it to list
                    messagesList.append(message)
                }
                
                self.sendMessagesLoadStatus(key: FirebaseRDBHelper.KEY_SENT_MESSAGES, status: true, errorMessage: " ", messages: messagesList)
            }
        })
    }
    
    func loadConversationMessages(userIds: Array<String>) {
        self.getMessagesReference()
        var messagesList = [MessageModel]()
        let userId = LocalChacheHelper.getUserFirKey()
        
        self.refMessages.queryOrdered(byChild: Constant.Firebase.KEY_MESSAGE_RECEIVER_ID).queryEqual(toValue: userId).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.exists() {
                //iterating through all the values
                for message in snapshot.children.allObjects as! [DataSnapshot] {
                    //getting values
                    let messageObject = message.value as? [String: AnyObject]
                    let messageId  = messageObject?[Constant.Firebase.KEY_MESSAGE_ID] as! String
                    let senderId  = messageObject?[Constant.Firebase.KEY_MESSAGE_SENDER_ID] as! String
                    let receiverId  = messageObject?[Constant.Firebase.KEY_MESSAGE_RECEIVER_ID] as! String
                    let fileLink  = messageObject?[Constant.Firebase.KEY_MESSAGE_FILE_LINK] as! String
                    let date  = messageObject?[Constant.Firebase.KEY_MESSAGE_DATE] as! TimeInterval
                    let status  = messageObject?[Constant.Firebase.KEY_MESSAGE_STATUS] as! Int
                    
                    //creating user object with model and fetched values
                    let message = MessageModel(id: messageId, senderId: senderId, receiverId: receiverId, fileLink: fileLink, date: date, status: status)
                    
                    //appending it to list
                    messagesList.append(message)
                }
            }
            
            self.sendMessagesLoadStatus(key: FirebaseRDBHelper.KEY_RECEIVED_MESSAGES, status: true, errorMessage: " ", messages: messagesList)
        })
    }
    
    func sendMessagesLoadStatus(key: Int, status: Bool, errorMessage: String, messages: Array<MessageModel>) {
        if delegateMessages != nil {
            switch(key){
                case FirebaseRDBHelper.KEY_RECEIVED_MESSAGES:
                    delegateMessages?.onReceivedMessagesLoaded(status: status, errorMessage: errorMessage, messages: messages)
                case FirebaseRDBHelper.KEY_SENT_MESSAGES:
                    delegateMessages?.onSentMessagesLoaded(status: status, errorMessage: errorMessage, messages: messages)
                default:
                    break
            }
        }
    }
}
