//
//  ConversationViewModel.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 6/9/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//


protocol ConversationViewModelDelegate {
    func onListItemClick(itemPosition: Int, messages: Array<MessageModel>)
}

class ConversationViewModel: NSObject {

    fileprivate let reuseIdentifierReceivedRecording = "cellReceivedRecording"
    fileprivate let reuseIdentifierSentRecording = "cellSentRecording"
    
    var delegate: ConversationViewModelDelegate?
    var firRDBHelper: FirebaseRDBHelper = FirebaseRDBHelper()
    var sentMessageList = [MessageModel]()
    var receivedMessageList = [MessageModel]()
    var allMessageList = [MessageModel]()
    var tableView: UITableView?
    
    var userObject: UserModel?
    var audioUtil = AudioUtil()
    
    func getUserName() -> String {
        return (userObject?.userName)!
    }
    
    func loadMessages() {
        firRDBHelper.delegateMessages = self
        firRDBHelper.loadReceivedMessages()
        firRDBHelper.loadSentMessages()
        self.audioUtil.delegate = self
    }
}

extension ConversationViewModel:UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allMessageList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // get a reference to our storyboard cell
        let message = self.allMessageList[indexPath.row]
        
        if message.senderId == LocalChacheHelper.getUserFirKey() {
            let cell: SentRecordingTableViewCell = (tableView.dequeueReusableCell(withIdentifier: reuseIdentifierSentRecording) as? SentRecordingTableViewCell)!
            
            cell.labelName.text = message.id
            cell.viewBackground.layer.cornerRadius = Constant.ConfigValues.CHAT_BUBBLE_CORNER_RADIUS
            cell.labelTime.text = DateTimeUtil.formatUnixTimeStamp(timeStamp: message.date)
            
            return cell
            
        } else {
            let cell: ReceivedRecordingTableViewCell = (tableView.dequeueReusableCell(withIdentifier: reuseIdentifierReceivedRecording) as? ReceivedRecordingTableViewCell)!
            
            cell.labelName.text = message.id
            cell.viewBackground.layer.cornerRadius = Constant.ConfigValues.CHAT_BUBBLE_CORNER_RADIUS
            cell.labelTime.text = DateTimeUtil.formatUnixTimeStamp(timeStamp: message.date)
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.sendOnItemClickEvent(position: indexPath.row, messages: self.allMessageList)
        self.audioUtil.downloadFileFromURLandPlay(urlString: self.allMessageList[indexPath.row].fileLink!)
    }
    
    func sendOnItemClickEvent(position: Int, messages: Array<MessageModel>){
        if delegate != nil {
            delegate?.onListItemClick(itemPosition: position, messages: messages)
        }
    }
    
    func updateMessages() {
        self.allMessageList.removeAll()
        self.allMessageList.append(contentsOf: self.receivedMessageList)
        self.allMessageList.append(contentsOf: self.sentMessageList)
        self.allMessageList = self.allMessageList.sorted{ $0.date < $1.date }
        self.tableView?.reloadData()
    }
}

extension ConversationViewModel: FirebaseRDBMessagesDelegate {
    
    func onReceivedMessagesLoaded(status: Bool, errorMessage: String, messages: [MessageModel]) {
        if status {
            self.receivedMessageList.removeAll()
            for message in messages {
                if message.senderId == self.userObject?.id {
                     self.receivedMessageList.append(message)
                }
            }
            self.updateMessages()
        }
    }
    
    func onSentMessagesLoaded(status: Bool, errorMessage: String, messages: [MessageModel]) {
        if status {
            self.sentMessageList.removeAll()
            for message in messages {
                if message.receiverId == self.userObject?.id {
                    self.sentMessageList.append(message)
                }
            }
            self.updateMessages()
        }
    }
}

extension ConversationViewModel: AudioUtilDelegate {

    func didAudioPlayFinished() {
        
    }
    
    func didAudioPlayInterrupted() {
        
    }
}

