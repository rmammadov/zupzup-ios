//
//  ConversationViewController.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 6/9/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import UIKit
import NWPusher

class ConversationViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Global declarations
    
    var user: UserModel?
    var modelConversation = ConversationViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        modelConversation.userObject = user
        self.tableView.delegate = modelConversation
        self.tableView.dataSource = modelConversation
        modelConversation.tableView = self.tableView
        self.setUi()
    }

    func setUi() {
        self.navigationItem.title = modelConversation.getUserName()
        modelConversation.loadMessages()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
