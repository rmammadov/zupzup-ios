//
//  Constant.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 4/24/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import Foundation

class Constant {
    
    //----------------- UserDefault keys----------------------------------------
    struct ConfigValues {
        static let DEVICE_OS: String = "IOS"
        static let NAME_OF_PERSISTENT_CONTAINER: String = "ZupZup"
        static let MIN_USERNAME_CHARS_COUNT: Int = 3
        static let MAX_DURATION_OF_RECORDING: TimeInterval = 10
        static let CHAT_BUBBLE_CORNER_RADIUS: CGFloat = 16
        static let MESSAGE_DATE_FORTMAT: String = "MM-dd HH:mm"
        static let MESSAGE_DATE_ABBREVIATION: String = "GMT"
    }
    
    //----------------- UserDefault keys----------------------------------------
    struct UserDefaultKeys {
        static let USER_FIR_KEY = "user_fir_key"
        static let USER_LOGGED_IN = "user_logged_id"
        static let USER_FIR_UID = "fir_user_uid"
        static let USER_NAME = "user_name"
    }
    
    //----------------- Keys ---------------------------------------------------
    struct Keys {
        static let IDENTIFIER_LOCAL_NOTIFICATON = "ZZLocalNotification"
    }
    
    //----------------- Firebase keys-------------------------------------------
    struct Firebase {
        static let DIR_RECORDINGS = "recordings"
        static let RECORDING_FILE_EXTENSION = ".m4a"
        static let DIR_USERS = "users"
        static let KEY_USER_ID = "id"
        static let KEY_USER_NAME = "username"
        static let KEY_USER_UID = "uid"
        static let KEY_NOTIFICATION_TOKEN = "notification_token"
        static let KEY_OS_NAME = "os_name"
        static let DIR_MESSAGES = "messages"
        static let KEY_MESSAGE_ID = "id"
        static let KEY_MESSAGE_SENDER_ID = "sender_id"
        static let KEY_MESSAGE_RECEIVER_ID = "receiver_id"
        static let KEY_MESSAGE_FILE_LINK = "file_link"
        static let KEY_MESSAGE_DATE = "date"
        static let KEY_MESSAGE_STATUS = "status"
    }
    
    //----------------- OpenEars keys-------------------------------------------
    struct OpenEars {
        static let NAME_OF_VOCABULARY = "users"
        static let VOICE_RECO_KEY_WORD = "Hey "
    }
    
    //----------------- OneSignal keys------------------------------------------
    struct OneSignal {
        static let APP_ID = "  "
    }
}
