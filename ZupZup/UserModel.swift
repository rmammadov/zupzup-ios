//
//  UserModel.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 6/2/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

class UserModel {

    var id: String?
    var uId: String?
    var userName: String?
    var apnsToken: String?
    
    init(id: String?, uId: String?, userName: String?, apnsToken: String?){
        self.id = id
        self.uId = uId
        self.userName = userName
        self.apnsToken = apnsToken
    }
}
