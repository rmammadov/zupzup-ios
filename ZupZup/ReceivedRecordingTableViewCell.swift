//
//  TableViewCell.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 6/14/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import UIKit

class ReceivedRecordingTableViewCell: UITableViewCell {

    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var labelTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
