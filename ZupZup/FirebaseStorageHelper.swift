//
//  FirebaseStorageHelper.swift
//  ZupZup
//
//  Created by Rahman Mammadov on 5/8/17.
//  Copyright © 2017 com.zupzup. All rights reserved.
//

import FirebaseStorage

protocol FirebaseStorageDelegate {
    func onFileUploaded(status: Bool, errorMessage: String, fileLink: String, key: String)
}


class FirebaseStorageHelper {
    
    var delegate: FirebaseStorageDelegate?
    
    func uploadToFireBase(recordedFile: URL, key: String) {
        
        // Create a reference to the file you want to upload
        let recordingRef = Storage.storage().reference().child(Constant.Firebase.DIR_RECORDINGS).child(key + Constant.Firebase.RECORDING_FILE_EXTENSION)
        
        // Upload the file to the path "images/rivers.jpg"
        _ = recordingRef.putFile(from: recordedFile, metadata: nil) { metadata, error in
            if error != nil {
                // Uh-oh, an error occurred!
                self.sendFileLoadStatus(status: false, errorMessage: " ", fileLink: " ", key: key)
            } else {
                // Metadata contains file metadata such as size, content-type, and download URL.
                let linkToFile = metadata!.downloadURL()
                self.sendFileLoadStatus(status: true, errorMessage: " ", fileLink: (linkToFile?.absoluteString)!, key: key)
            }
        }        
    }
    
    func sendFileLoadStatus(status: Bool, errorMessage: String, fileLink: String, key: String) {
        if delegate != nil {
            delegate?.onFileUploaded(status: status, errorMessage: errorMessage, fileLink: fileLink, key: key)
        }
    }
}
